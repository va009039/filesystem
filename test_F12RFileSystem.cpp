// test_F12RFileSystem.cpp 2015/10/31
#ifdef TEST_F12R
#include "mbed.h"
#include "MemStorage.h"
#include "F12RFileSystem.h"

RawSerial pc(USBTX,USBRX);
DigitalOut led1(LED1);

int main() {
    pc.baud(115200);
    pc.puts(__FILE__);
    pc.puts("\r\n");

    MemStorage storage;
    F12RFileSystem local(&storage, "local");
    local.format();
   
    FILE* fp = fopen("/local/test.txt", "a");
    MBED_ASSERT(fp);
    fputs("Hello,world! 1556\r\n", fp);
    fclose(fp);

    fp = fopen("/local/test.txt", "r");
    MBED_ASSERT(fp);
    for(;;) {
        int c = fgetc(fp);
        if (c == EOF) {
            break;
        }
        pc.putc(c);
    }
    fclose(fp);

    for(;;) {
        led1 = !led1;
        wait_ms(200);
    }
}
#endif // TEST_F12R

