StorageInterfaceでストレージとファイルシステムを分離する試み

### Filesystem ###
* https://developer.mbed.org/users/va009039/code/F12RFileSystem/
* https://developer.mbed.org/users/va009039/code/F32RFileSystem/
* https://developer.mbed.org/users/va009039/code/F32FileSystem/

### Storage ###
* https://developer.mbed.org/users/va009039/code/RAMStorage/
* https://developer.mbed.org/users/va009039/code/SDStorage/

### Misc ###
* https://developer.mbed.org/users/va009039/code/ROMSLOT/