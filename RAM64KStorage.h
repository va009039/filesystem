// RAM64KStorage.h 2016/3/27
#pragma once
#include "StorageInterface.h"
#include "fsdebug.h"

namespace mbed
{
class RAM64KStorage : public StorageInterface
{
public:
    RAM64KStorage(int size_ = 65536):size(size_) {
        mem = new uint8_t[size];
    }

    virtual ~RAM64KStorage() {
        delete[] mem;
    }

    virtual int Read(uint64_t offset, uint8_t* buffer, uint32_t size) {
        FS_DBG("offset=%d(%d,%d) size=%d", (uint32_t)offset, (uint32_t)offset/512, (uint32_t)offset%512, size);
        memcpy(buffer, mem + offset, size);
        return 0;
    }

    virtual int Write(uint64_t offset, const uint8_t* buffer, uint32_t size) {
        FS_DBG("offset=%d(%d,%d) size=%d", (uint32_t)offset, (uint32_t)offset/512, (uint32_t)offset%512, size);
        memcpy(mem + offset, buffer, size);
        return 0;
    }

    virtual uint64_t Size() { return size; }

private:
    int size;
    uint8_t* mem;
};
} // namespace mbed

