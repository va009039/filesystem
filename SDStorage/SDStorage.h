/* mbed Microcontroller Library
 * Copyright (c) 2006-2017 ARM Limited
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef MBED_SD_STORAGE_H
#define MBED_SD_STORAGE_H

#include "StorageInterface.h"
#include <stdint.h>

/** Access the filesystem on an SD Card using SPI
 *
 * @code
 * #include "mbed.h"
 * #include "F32RFileSystem.h"
 *
 * SDStorage storaged(p5, p6, p7, p12); // mosi, miso, sclk, cs
 * F32FileSystem sd(&storage, "sd");
 *
 * int main() {
 *     FILE *fp = fopen("/sd/myfile.txt", "w");
 *     fprintf(fp, "Hello World!\n");
 *     fclose(fp);
 * }
 */
class SDStorage : public StorageInterface {
public:
    /** Create the File System for accessing an SD Card using SPI
     *
     * @param mosi SPI mosi pin connected to SD Card
     * @param miso SPI miso pin conencted to SD Card
     * @param sclk SPI sclk pin connected to SD Card
     * @param cs   DigitalOut pin used as SD Card chip select
     */
    SDStorage(PinName mosi, PinName miso, PinName sclk, PinName cs);
    virtual int Read(uint64_t offset, uint8_t* buffer, uint32_t size);
    virtual int Write(uint64_t offset, const uint8_t* buffer, uint32_t size);
    virtual uint64_t Size();

private:
    bool inited;
    int disk_initialize();
    int disk_status();
    int disk_read(uint8_t* buffer, uint64_t block_number, uint8_t count);
    int disk_write(const uint8_t* buffer, uint64_t block_number, uint8_t count);
    int disk_sync();
    uint64_t disk_sectors();

    int _cmd(int cmd, int arg);
    int _cmdx(int cmd, int arg);
    int _cmd8();
    int _cmd58();
    int initialise_card();
    int initialise_card_v1();
    int initialise_card_v2();

    int _read(uint8_t * buffer, uint32_t length);
    int _write(const uint8_t *buffer, uint32_t length);
    uint64_t _sd_sectors();
    uint64_t _sectors;

    void set_init_sck(uint32_t sck) { _init_sck = sck; }
    // Note: The highest SPI clock rate is 20 MHz for MMC and 25 MHz for SD
    void set_transfer_sck(uint32_t sck) { _transfer_sck = sck; }
    uint32_t _init_sck;
    uint32_t _transfer_sck;

    SPI _spi;
    DigitalOut _cs;
    int cdv;
    int _is_initialized;
};

#endif // MBED_SD_STORAGE_H
