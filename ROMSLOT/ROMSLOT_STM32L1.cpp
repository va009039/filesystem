// ROMSLOT_STM32L1.cpp 2016/4/9
#if defined(TARGET_STM32L1)
#include "ROMSLOT.h"

ROMSLOT::ROMSLOT() {
    base = 0x20000;
}

uint32_t ROMSLOT::New(uint32_t size) {
    uint32_t addr = base;
    base += (size + FLASH_PAGE_SIZE - 1) / FLASH_PAGE_SIZE * FLASH_PAGE_SIZE;
    return addr;
}

static bool is_base(uint32_t addr) {
    return addr % FLASH_PAGE_SIZE == 0;
}

bool ROMSLOT::Write(uint32_t addr, const uint8_t buf[], uint32_t size) {
    uint32_t data;
    MBED_ASSERT(addr % sizeof(data) == 0);
    MBED_ASSERT(size % sizeof(data) == 0);
    if (memcmp((uint8_t*)addr, buf, size) == 0) { // skip ?
        return true;
    }
    HAL_FLASH_Unlock();
    bool result = true;
    for(uint32_t n = 0; n < size; n += sizeof(data)) {
        if (is_base(addr + n)) {
            FLASH_EraseInitTypeDef Erase;
            Erase.NbPages = 1;
            Erase.TypeErase = TYPEERASEDATA_BYTE;
            Erase.PageAddress = addr + n;
            uint32_t PageError = 0;
            HAL_StatusTypeDef status = HAL_FLASHEx_Erase(&Erase, &PageError);
            MBED_ASSERT(status == HAL_OK);
            if (status != HAL_OK) {
                result = false;
                break;
            }
        }
        memcpy(&data, buf + n, sizeof(data));
        HAL_StatusTypeDef status = HAL_FLASH_Program(TYPEPROGRAM_WORD, addr + n, (uint64_t)data);
        MBED_ASSERT(status == HAL_OK);
        if (status != HAL_OK) {
            result = false;
            break;
        }
    }
    HAL_FLASH_Lock();
    return result;
}
#endif // TARGET_STM32L1

