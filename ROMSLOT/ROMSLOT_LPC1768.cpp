// ROMSLOT_LPC1768.cpp 2016/4/9
#if defined(TARGET_LPC1768)
#include "ROMSLOT.h"

static const uint32_t FLASH_SECTOR_BASE = 0x10000;
static const uint32_t FLASH_SECTOR_SIZE = 0x8000; // 32 Kbytes

#if defined(IAP_DEBUG)
#define IAP_DBG(...) do{fprintf(stderr,"[%s@%d] ",__PRETTY_FUNCTION__,__LINE__);fprintf(stderr,__VA_ARGS__);fprintf(stderr,"\r\n");} while(0)
#else
#define IAP_DBG(...) while(0)
#endif
#define IAP_ASSERT(A) MBED_ASSERT(A)

#define IAP_LOCATION 0x1fff1ff1
typedef void (*IAP)(uint32_t* command, uint32_t* result);

uint32_t iap(uint32_t cmd, uint32_t p0 = 0, uint32_t p1 = 0, uint32_t p2 = 0, uint32_t p3 = 0) {
    uint32_t command[5] = {cmd, p0, p1, p2, p3};
    uint32_t result[4];
    IAP iap_entry = (IAP)IAP_LOCATION;
    IAP_DBG("command: %x %x %x %x %x\n", command[0], command[1], command[2], command[3], command[4]);
    iap_entry(command, result);
    IAP_DBG("result: %x %x %x %x\n", result[0], result[1], result[2], result[3]);
    return result[0];
}

ROMSLOT::ROMSLOT() {
    base = FLASH_SECTOR_BASE;
}

uint32_t ROMSLOT::New(uint32_t size) {
    uint32_t addr = base;
    base += (size + FLASH_SECTOR_SIZE - 1) / FLASH_SECTOR_SIZE * FLASH_SECTOR_SIZE;
    return addr;
}

static bool is_base(uint32_t addr) {
    return addr % FLASH_SECTOR_SIZE == 0;
}

static uint32_t sector(uint32_t addr) {
    MBED_ASSERT(addr >= FLASH_SECTOR_BASE);
    uint32_t s = (addr - FLASH_SECTOR_BASE) / FLASH_SECTOR_SIZE + 16;
    return s;
}

bool ROMSLOT::Write(uint32_t addr, const uint8_t buf[], uint32_t size) {
    IAP_ASSERT(addr%256 == 0);
    IAP_ASSERT(size == 256);
    if (memcmp((uint8_t*)addr, buf, size) == 0) { // skip ?
        return true;
    }
    uint8_t temp[256];
    memcpy(temp, buf, sizeof(temp));
    IAP_ASSERT(addr >= 0x10000);
    uint32_t sectorNo = sector(addr);
    IAP_ASSERT(sectorNo >= 16);
    uint32_t cclk = SystemCoreClock / 1000;
    if (is_base(addr)) {
        if (iap(50, sectorNo, sectorNo) != 0) { // prepare
            return false;
        }
        if (iap(52, sectorNo, sectorNo, cclk) != 0) { // erase
            return false;
        }
    }
    if (iap(50, sectorNo, sectorNo) != 0) { // prepare
        return false;
    }
    if (iap(51, addr, (uint32_t)temp, size, cclk) != 0) { // Copy RAM to flash
        return false;
    }
    if (iap(56, addr, (uint32_t)temp, size) != 0) { // Compare
        return false;
    }
    return true;
}
#endif // TARGET_LPC1768

