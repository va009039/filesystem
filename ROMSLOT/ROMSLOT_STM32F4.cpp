// ROMSLOT_STM32F4.cpp 2016/4/9
#if defined(TARGET_STM32F4)
#include "ROMSLOT.h"

static const uint32_t FLASH_SECTOR_SIZE = 0x20000; // 128 Kbytes

ROMSLOT::ROMSLOT() {
    base = 0x20000;
}

uint32_t ROMSLOT::New(uint32_t size) {
    uint32_t addr = base;
    base += (size + FLASH_SECTOR_SIZE - 1) / FLASH_SECTOR_SIZE * FLASH_SECTOR_SIZE;
    return addr;
}

static bool is_base(uint32_t addr) {
    return addr % FLASH_SECTOR_SIZE == 0;
}

static uint32_t sector(uint32_t addr) {
    MBED_ASSERT(addr >= 0x20000 && addr <= 0x60000);
    uint32_t s = (addr - 0x20000) / FLASH_SECTOR_SIZE + 5; // sector 5,6 und 7
    return s;
}

bool ROMSLOT::Write(uint32_t addr, const uint8_t buf[], uint32_t size) {
    uint32_t data;
    MBED_ASSERT(addr % sizeof(data) == 0);
    MBED_ASSERT(size % sizeof(data) == 0);
    if (memcmp((uint8_t*)addr, buf, size) == 0) { // skip ?
        return true;
    }
    HAL_FLASH_Unlock();
    bool result = true;
    for(uint32_t n = 0; n < size; n += sizeof(data)) {
        if (is_base(addr + n)) {
            FLASH_EraseInitTypeDef Erase;
            Erase.TypeErase = FLASH_TYPEERASE_SECTORS;
            Erase.VoltageRange = FLASH_VOLTAGE_RANGE_1;
            Erase.NbSectors = 1;
            Erase.Sector = sector(addr + n);
            uint32_t PageError = 0;
            HAL_StatusTypeDef status = HAL_FLASHEx_Erase(&Erase, &PageError);
            MBED_ASSERT(status == HAL_OK);
            if (status != HAL_OK) {
                result = false;
                break;
            }
        }
        memcpy(&data, buf + n, sizeof(data));
        HAL_StatusTypeDef status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, addr + n, data);
        MBED_ASSERT(status == HAL_OK);
        if (status != HAL_OK) {
            result = false;
            break;
        }
    }
    HAL_FLASH_Lock();
    return result;
}
#endif // TARGET_STM32F4

