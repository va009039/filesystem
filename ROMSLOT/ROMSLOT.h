// ROMSLOT.h 2016/4/9
#include "mbed.h"

class ROMSLOT {
public:
    ROMSLOT();
    uint32_t New(uint32_t size);
    bool Write(uint32_t addr, const uint8_t* buf, uint32_t size);

private:
    uint32_t base;
};
