/* mbed Microcontroller Library
 * Copyright (c) 2006-2017 ARM Limited
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "F32RDirHandle.h"
#include "fsdebug.h"

using namespace mbed;

F32RDirHandle::F32RDirHandle(F32RFileSystem& fs_, cluster_t cluster_):fs(fs_) {
    first_cluster = cluster_;
    cluster = cluster_;
    pos = 0;
}

int F32RDirHandle::closedir() {
    delete this;
    return 0;
}

static void sfn_to_name(const uint8_t* buf, char* name) {
    for(int i = 0; i < 8; i++) {
        char c = buf[i];
        if (c == '\x20') {
            break;
        }
        *name++ = c;
    }
    for(int i = 8; i < 8+3; i++) {
        char c = buf[i];
        if (c == '\x20') {
            break;
        } else if (i == 8) {
            *name++ = '.';
        }
        *name++ = c;
    }
    *name = '\0';
}

struct dirent *F32RDirHandle::readdir() {
    FS_DBG("pos=%d cluster=%d", pos, cluster);
    while(cluster < 0x0ffffff8) {
        FS_TEST_ASSERT(cluster >= 2);
        uint8_t buf[32];
        fs.storage->Read(fs.base_data + (cluster-2) * fs.cluster_size + fs.cluster_head(pos), buf, sizeof(buf));
        FS_DBG_HEX(buf, sizeof(buf));
        pos += 32;
        if (fs.cluster_head(pos) == 0) {
            cluster = fs.fat_read(cluster);
        }
        if (buf[0] == 0x00) {
            break;
        } else if (buf[0] != 0xe5 && buf[11] == 0x20) {
            sfn_to_name(buf, cur_entry.d_name);
            return &cur_entry;
        }
    }
    return NULL;
}

void F32RDirHandle::rewinddir() {
    cluster = first_cluster;
    pos = 0;
}

off_t F32RDirHandle::telldir() {
    return pos;
}

void F32RDirHandle::seekdir(off_t location_) {
    // TODO
}

