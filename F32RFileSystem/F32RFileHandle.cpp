/* mbed Microcontroller Library
 * Copyright (c) 2006-2016 ARM Limited
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <algorithm>
#include "F32RFileHandle.h"
#include "fsdebug.h"

F32RFileHandle::F32RFileHandle(F32RFileSystem& fs_, uint32_t d):fs(fs_) {
    pos = 0;
    cluster = fs.storage_peek(d + 20, 2)<<16|fs.storage_peek(d + 26, 2);
    file_length = fs.storage_peek(d + 28, 4);
    FS_DBG("cluster=%d", cluster);
    FS_DBG("file_length=%d", file_length);
}

int F32RFileHandle::close() {
    delete this;
    return 0;
}

ssize_t F32RFileHandle::write(const void* buffer, size_t length) {
    return -1;
}

ssize_t F32RFileHandle::read(void* buffer, size_t length) {
    FS_DBG("length=%d", length);
    ssize_t n = 0;
    while(n < length && pos < file_length) {
        uint32_t chunk = std::min((filesize_t)length - n, file_length - pos);
        chunk = std::min(chunk, fs.cluster_tail(pos));
        FS_TEST_ASSERT(cluster >= 2 && cluster < 0x0ffffff8);
        fs.storage->Read(fs.base_data + (uint64_t)(cluster-2)*fs.cluster_size + fs.cluster_head(pos), (uint8_t*)buffer + n, chunk);
        pos += chunk;
        if (fs.cluster_head(pos) == 0) {
            cluster = fs.fat_read(cluster);
        }
        n += chunk;
    }
    return n;
}

int F32RFileHandle::isatty() {
    return 0;
}

off_t F32RFileHandle::lseek(off_t position, int whence) {
    FS_DBG("position=%d whence=%d", (uint32_t)position, whence);
    return -1;
}

int F32RFileHandle::fsync() {
    return 0;
}

off_t F32RFileHandle::flen() {
    FS_DBG("file_length=%d", file_length);
    return file_length;
}
