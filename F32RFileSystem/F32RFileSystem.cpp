/* mbed Microcontroller Library
 * Copyright (c) 2006-2017 ARM Limited
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "F32RFileSystem.h"
#include "F32RFileHandle.h"
#include "F32RDirHandle.h"
#include "fsdebug.h"
#include "mbed_debug.h"

F32RFileSystem::F32RFileSystem(StorageInterface* storage_, const char* n)
    : FileSystemLike(n), storage(storage_), mounted(false) {
}

F32RFileSystem::~F32RFileSystem() {
}

static bool to_sfn(const char* name, uint8_t* buf) {
    memset(buf, '\x20', 8+3);
    for(int i = 0; i < 8; i++) {
        char c = *name++;
        if (c == '\0') {
            return true;
        } else if (c == '.') {
            break;
        } else {
            buf[i] = toupper(c);
        }
    }
    if (*name == '.') {
        name++;
    }
    for(int i = 8; i < 11; i++) {
        char c = *name++;
        if (c == '\0') {
            break;
        } else {
            buf[i] = toupper(c);
        }
    }
    return true;
}

FileHandle *F32RFileSystem::open(const char* name, int flags) {
    FS_DBG("name=[%s] flags=%d", name, flags);
    if (mount() != 0) {
        return NULL;
    }

#if 1
    uint8_t fat[32];
    storage->Read(base_fat, fat, sizeof(fat));
    FS_DBG_HEX(fat, sizeof(fat));
#endif

    uint8_t sfn[8+3];
    to_sfn(name, sfn);
    FS_DBG_HEX(sfn, sizeof(sfn));
    cluster_t cluster = root_directory_first_cluster;
    int pos = 0;
    while(cluster < 0x0ffffff8) {
        FS_TEST_ASSERT(cluster >= 2);
        uint32_t dir_pos = base_data + (cluster-2)*cluster_size + cluster_head(pos);
        uint8_t buf[32];
        storage->Read(dir_pos, buf, sizeof(buf));
        FS_DBG_HEX(buf, sizeof(buf));
        if (buf[0] == 0x00) {
            return NULL;
        } else if (memcmp(buf, sfn, sizeof(sfn)) == 0) {
            return new F32RFileHandle(*this, dir_pos);
        }
        pos += 32;
        if (cluster_head(pos) == 0) {
            cluster = fat_read(cluster);
        }
    }
    return NULL;
}

int F32RFileSystem::remove(const char *filename) {
    return -1;
}

int F32RFileSystem::rename(const char *oldname, const char *newname) {
    return -1;
}

int F32RFileSystem::format() {
    return -1;
}

DirHandle *F32RFileSystem::opendir(const char *name) {
    FS_DBG("name=[%s]", name);

    if (mount() != 0) {
        return NULL;
    }
    return new F32RDirHandle(*this, root_directory_first_cluster);
}

int F32RFileSystem::mkdir(const char *name, mode_t mode) {
    return -1;
}

int F32RFileSystem::mount() {
    if (storage_peek(0 * 512 + 510, 2) != 0xaa55) {
        return -1;
    }
    uint32_t lba_offset = 0;
    struct { // +446
        uint8_t flag; // +0
        uint8_t first_sector_chs[3]; // +1
        uint8_t type; // +4
        uint8_t last_sector_chs[3]; // +5
        uint32_t sector_start; // +8
        uint32_t sector_count; // +12
    } partition_entry;
    storage->Read(446, (uint8_t*)&partition_entry, sizeof(partition_entry));
    fs_type = partition_entry.type;
    FS_DBG_HEX((uint8_t*)&partition_entry, sizeof(partition_entry));
    FS_DBG("fs_type=%02x", fs_type);
    if (!is_fat32()) {
        debug("fat32 only.");
        return -1;
    }
    if (is_fat12() || is_fat16() || is_fat32()) {
        lba_offset = partition_entry.sector_start * 512;
        if (storage_peek(lba_offset + 510, 2) != 0xaa55) {
            return -1;
        }
    }

#if 1
    FS_DBG("lba_offset=%d", lba_offset);
    uint8_t buf[512];
    storage->Read(lba_offset, buf, sizeof(buf));
    FS_DBG_HEX(buf, sizeof(buf));
#endif

    if (storage_peek(lba_offset + 11, 2) != 512) { // verify sector size
        return -1;
    }
    cluster_size = storage_peek(lba_offset + 13, 1) * 512;
    sector_t number_of_reserved_sectors = storage_peek(lba_offset + 14, 2);
    base_fat = lba_offset + number_of_reserved_sectors * 512;
    uint32_t number_of_fats = storage_peek(lba_offset + 16, 1);
    FS_TEST_ASSERT(number_of_fats == 1 || number_of_fats == 2);
    // fat32
    sector_t number_of_per_fat = storage_peek(lba_offset + 0x24, 4);
    root_directory_first_cluster = storage_peek(lba_offset + 0x2c, 4);
    root_directory_size = fat32_count(root_directory_first_cluster) * cluster_size;

    base_data = base_fat + number_of_per_fat * number_of_fats * 512;

    FS_DBG("number_of_reserved_sectors=%d", number_of_reserved_sectors);
    FS_DBG("number_of_fats=%d", number_of_fats);

    FS_DBG("fat32: number_of_per_fat=%d", number_of_per_fat);
    FS_DBG("fat32: root_directory_first_cluster=%d", root_directory_first_cluster);
    FS_DBG("fat32: root_directory_size=%d", root_directory_size);

    FS_DBG("cluster_size=%d", cluster_size);
    FS_DBG("base_fat=%d", base_fat);
    FS_DBG("base_data=%d", base_data);

    mounted = true;
    return 0;
}

int F32RFileSystem::unmount() {
    mounted = false;
    return 0;
}

uint32_t F32RFileSystem::storage_peek(uint32_t offset, int n) {
    uint8_t buf[n];
    storage->Read(offset, buf, sizeof(buf));
    uint32_t val = 0;
    for(int i = 0; i < n; i++) {
        val |= buf[i]<<(i*8);
    }
    return val;
}

cluster_t F32RFileSystem::fat_read(cluster_t index) {
    if (is_fat32()) {
        return storage_peek(base_fat + index*4, 4);
    } else if (is_fat16()) {
        return storage_peek(base_fat + index*2, 2);
    }
    FS_TEST_ASSERT(is_fat12());
    int i = index / 2 * 3 + (index&1);
    cluster_t next = storage_peek(base_fat + i, 2);
    if (index & 1) {
        next >>= 4;
    } else {
        next &= 0xfff;
    }
    return next;
}

int F32RFileSystem::fat32_count(cluster_t cluster) {
    int count = 0;
    for(; cluster >= 2 && cluster < 0x0ffffff8; count++) {
        cluster = fat_read(cluster);
    }
    return count;
}

