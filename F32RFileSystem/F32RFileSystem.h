/* mbed Microcontroller Library
 * Copyright (c) 2006-2016 ARM Limited
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef MBED_F32R_FILESYSTEM_H
#define MBED_F32R_FILESYSTEM_H

#include "FileSystemLike.h"
#include "FileHandle.h"
#include <stdint.h>
#include "StorageInterface.h"

typedef uint32_t sector_t;
typedef uint32_t cluster_t;
typedef uint32_t filesize_t;

using namespace mbed;

/**
 * F32RFileSystem
 */
class F32RFileSystem : public FileSystemLike {
public:
    F32RFileSystem(StorageInterface* storage_, const char* n);
    virtual ~F32RFileSystem();

    /**
     * Opens a file on the filesystem
     */
    virtual FileHandle *open(const char* name, int flags);
    
    /**
     * Removes a file path
     */
    virtual int remove(const char *filename);
    
    /**
     * Renames a file
     */
    virtual int rename(const char *oldname, const char *newname);
    
    /**
     * Formats a logical drive, FDISK artitioning rule, 512 bytes per cluster
     */
    virtual int format();
    
    /**
     * Opens a directory on the filesystem
     */
    virtual DirHandle *opendir(const char *name);
    
    /**
     * Creates a directory path
     */
    virtual int mkdir(const char *name, mode_t mode);
    
    /**
     * Mounts the filesystem
     */
    virtual int mount();
    
    /**
     * Unmounts the filesystem
     */
    virtual int unmount();

private:
    friend class F32RFileHandle;
    friend class F32RDirHandle;
    uint32_t storage_peek(uint32_t offset, int n);
    cluster_t fat_read(cluster_t index);
    int fat32_count(cluster_t cluster); // fat32
    StorageInterface* storage;
    uint32_t base_fat;
    uint64_t base_data;
    uint32_t cluster_size;
    uint32_t cluster_head(uint32_t pos) const { return pos % cluster_size; }
    uint32_t cluster_tail(uint32_t pos) const { return cluster_size - cluster_head(pos); }
    union {
        cluster_t root_directory_first_cluster;
        uint32_t base_dir; // fat12/fat16
    };
    filesize_t root_directory_size;
    int fs_type;
    bool is_fat12() const { return fs_type == 0x01; }
    bool is_fat16() const { return fs_type == 0x04 || fs_type == 0x06 || fs_type == 0x0e; }
    bool is_fat32() const { return fs_type == 0x0b || fs_type == 0x0c; }
    bool mounted;
};

#endif // MBED_F32R_FILESYSTEM_H
