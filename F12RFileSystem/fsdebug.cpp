#include "mbed.h"
#include "fsdebug.h"

void _fs_dbg_head(const char* pp, int line) { fprintf(stderr,"[DBG:%s@%d] ", pp, line); }
void _fs_dbg1_head(const char* pp, int line) { fprintf(stderr,"[DBG1:%s@%d] ", pp, line); }
void _fs_dbg2_head(const char* pp, int line) { fprintf(stderr,"[DBG2:%s@%d] ", pp, line); }
void _fs_dbg3_head(const char* pp, int line) { fprintf(stderr,"[DBG3:%s@%d] ", pp, line); }
void _ref_dbg_head(const char* pp, int line) { fprintf(stderr,"[REF:%s@%d] ", pp, line); }
void _fs_dbg_tail() { fprintf(stderr,"\r\n"); }
void _fs_dbg_hex(uint8_t* data, int size) {
    char ascbuf[16+1];
    for(int i = 0; i < size; i++) {
        if (i%16 == 0) {
            fprintf(stderr, "%3d:", i);
        }
        fprintf(stderr, " %02x", data[i]);
        char c = data[i];
        ascbuf[i%16] = (isprint(c) && c <= 0x7f) ? c : '.';
        if (i%16 == 15) {
            ascbuf[16] = '\0';
            fprintf(stderr, " %s\n", ascbuf);
        }
    }
    fprintf(stderr, "\n");
}

void _fs_test_assert(const char* pp, int line, const char* expr) {
    fprintf(stderr, "\n\n%s@%d %s ASSERT!!!\n\n", pp, line, expr);
    exit(1);
    /* NOTREACHED */
}

void _fs_test_assert0(const char* pp, int line) {
    fprintf(stderr, "\n\n%s@%d ASSERT!!!\n\n", pp, line);
    exit(1);
    /* NOTREACHED */
}


