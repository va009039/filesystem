/* mbed Microcontroller Library
 * Copyright (c) 2006-2016 ARM Limited
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "F12RDirHandle.h"
#include "fsdebug.h"

using namespace mbed;

F12RDirHandle::F12RDirHandle(F12RFileSystem& fs_):fs(fs_) {
    location = 0;
}

int F12RDirHandle::closedir() {
    delete this;
    return 0;
}

static void sfn_to_name(const uint8_t* buf, char* name) {
    for(int i = 0; i < 8; i++) {
        char c = buf[i];
        if (c == '\x20') {
            break;
        }
        *name++ = c;
    }
    for(int i = 8; i < 8+3; i++) {
        char c = buf[i];
        if (c == '\x20') {
            break;
        } else if (i == 8) {
            *name++ = '.';
        }
        *name++ = c;
    }
    *name = '\0';
}

struct dirent *F12RDirHandle::readdir() {
    for( ; location < fs.max_root_dir_entries; location++) {
        uint8_t buf[8+3];
        fs.storage->Read(fs.base_dir + location * 32, buf, sizeof(buf));
        if (buf[0] == 0xe5) {
            ;
        } else if (buf[0] == 0x00) {
            break;
        } else {
            sfn_to_name(buf, cur_entry.d_name);
            location++;
            return &cur_entry;
        }
    }
    return NULL;
}

void F12RDirHandle::rewinddir() {
    location = 0;
}

off_t F12RDirHandle::telldir() {
    return location;
}

void F12RDirHandle::seekdir(off_t location_) {
    location = location_;
}

