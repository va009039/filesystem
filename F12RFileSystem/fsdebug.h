#pragma once
#include <cctype>

#if 1 && defined(FS_DEBUG)
#define FS_DBG(...) do{_fs_dbg_head(__PRETTY_FUNCTION__,__LINE__);fprintf(stderr,__VA_ARGS__);_fs_dbg_tail();} while(0)
#define FS_DBG_HEX(A,B) do{_fs_dbg_hex(A, B);}while(0)
#define FS_ERR(...) do{fprintf(stderr,"[ERR:%s@%d] ",__PRETTY_FUNCTION__,__LINE__);fprintf(stderr,__VA_ARGS__);fprintf(stderr,"\r\n");} while(0)
void _fs_dbg_head(const char* pp, int line);
void _fs_dbg_tail();
void _fs_dbg_hex(uint8_t* data, int size);
void _fs_dbg(const char* pp, int line, const char* format, ...);
#else
#define FS_DBG(...) while(0)
#define FS_DBG_HEX(A,B) while(0)
#define FS_ERR(...) while(0)
#endif

#if 1 && defined(FS_DEBUG2)
#define FS_DBG2(...) do{_fs_dbg2_head(__PRETTY_FUNCTION__,__LINE__);fprintf(stderr,__VA_ARGS__);_fs_dbg_tail();} while(0)
#define FS_DBG2_HEX(A,B) do{_fs_dbg_hex(A, B);}while(0)
#define FS_ERR2(...) do{fprintf(stderr,"[ERR:%s@%d] ",__PRETTY_FUNCTION__,__LINE__);fprintf(stderr,__VA_ARGS__);fprintf(stderr,"\r\n");} while(0)
void _fs_dbg2_head(const char* pp, int line);
void _fs_dbg_tail();
void _fs_dbg_hex(uint8_t* data, int size);
#else
#define FS_DBG2(...) while(0)
#define FS_DBG2_HEX(A,B) while(0)
#define FS_ERR2(...) while(0)
#endif

#if defined(REF_DEBUG)
#define REF_DBG(...) do{_ref_dbg_head(__PRETTY_FUNCTION__,__LINE__);fprintf(stderr,__VA_ARGS__);_fs_dbg_tail();} while(0)
void _ref_dbg_head(const char* pp, int line);
void _fs_dbg_tail();
#else
#define REF_DBG(...) while(0)
#endif

#if 1 && defined(FS_TEST)
//#define FS_TEST_ASSERT(A) while(!(A)){_fs_test_assert(__PRETTY_FUNCTION__, __LINE__, #A);}
#define FS_TEST_ASSERT(A) while(!(A)){_fs_test_assert0(__PRETTY_FUNCTION__, __LINE__);}
void _fs_test_assert(const char* pp, int line, const char* expr);
void _fs_test_assert0(const char* pp, int line);
#else
#define FS_TEST_ASSERT(A) while(0)
#endif

