// StorageInterface.h 2016/3/27
#pragma once
#include "mbed.h"

namespace mbed
{
class StorageInterface {
public:
    // read storage, return 0 if ok
    virtual int Read(uint64_t offset, uint8_t* buffer, uint32_t size) = 0;
    // write storage, return 0 if ok
    virtual int Write(uint64_t offset, const uint8_t* buffer, uint32_t size) = 0;
    // return storage size
    virtual uint64_t Size() = 0;
};
} // namespace mbed

