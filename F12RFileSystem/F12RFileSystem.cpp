/* mbed Microcontroller Library
 * Copyright (c) 2006-2017 ARM Limited
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "F12RFileSystem.h"
#include "F12RFileHandle.h"
#include "F12RDirHandle.h"
#include "fsdebug.h"

F12RFileSystem::F12RFileSystem(StorageInterface* storage_, const char* n)
    : FileSystemLike(n), storage(storage_), mounted(false) {
}

F12RFileSystem::~F12RFileSystem() {
}

static bool to_sfn(const char* name, uint8_t* buf) {
    memset(buf, '\x20', 8+3);
    for(int i = 0; i < 8; i++) {
        char c = *name++;
        if (c == '\0') {
            return true;
        } else if (c == '.') {
            break;
        } else {
            buf[i] = toupper(c);
        }
    }
    if (*name == '.') {
        name++;
    }
    for(int i = 8; i < 11; i++) {
        char c = *name++;
        if (c == '\0') {
            break;
        } else {
            buf[i] = toupper(c);
        }
    }
    return true;
}

FileHandle *F12RFileSystem::open(const char* name, int flags) {
    FS_DBG("name=[%s] flags=%d", name, flags);
    if (mount() != 0) {
        return NULL;
    }

#if 1
    uint8_t fat12[16];
    storage->Read(base_fat, fat12, sizeof(fat12));
    FS_DBG_HEX(fat12, sizeof(fat12));
#endif

    uint8_t sfn[8+3];
    to_sfn(name, sfn);
    for(int i = 0; i < max_root_dir_entries; i++) {
        uint8_t buf[sizeof(sfn)];
        storage->Read(base_dir + i * 32, buf, sizeof(buf));
        FS_DBG_HEX(buf, sizeof(buf));
        if (buf[0] == 0x00) {
            return NULL;
        } else if (memcmp(buf, sfn, sizeof(sfn)) == 0) {
            return new F12RFileHandle(*this, base_dir + i * 32);
        }
    }
    return NULL;
}

int F12RFileSystem::remove(const char *filename) {
    return -1;
}

int F12RFileSystem::rename(const char *oldname, const char *newname) {
    return -1;
}

int F12RFileSystem::format() {
    return -1;
}

DirHandle *F12RFileSystem::opendir(const char *name) {
    FS_DBG("name=[%s]", name);

    if (mount() != 0) {
        return NULL;
    }
    return new F12RDirHandle(*this);
}

int F12RFileSystem::mkdir(const char *name, mode_t mode) {
    return -1;
}

int F12RFileSystem::mount() {
#if 1
    uint8_t buf[512];
    storage->Read(0, buf, sizeof(buf));
    FS_DBG_HEX(buf, sizeof(buf));
#endif

    if (storage_peek(0 * 512 + 510, 2) != 0xaa55) {
        return -1;
    }
    uint32_t lba_offset = 0;
    struct { // +446
        uint8_t flag; // +0
        uint8_t first_sector_chs[3]; // +1
        uint8_t type; // +4
        uint8_t last_sector_chs[3]; // +5
        uint32_t sector_start; // +8
        uint32_t sector_count; // +12
    } partition_entry;
    storage->Read(446, (uint8_t*)&partition_entry, sizeof(partition_entry));
    if (partition_entry.type == 0x01) {
        lba_offset = partition_entry.sector_start * 512;
        if (storage_peek(lba_offset + 510, 2) != 0xaa55) {
            return -1;
        }
    }

#if 1
    FS_DBG("lba_offset=%d", lba_offset);
    storage->Read(lba_offset, buf, sizeof(buf));
    FS_DBG_HEX(buf, sizeof(buf));
#endif

    if (storage_peek(lba_offset + 11, 2) != 512) {
        return -1;
    }
    cluster_size = storage_peek(lba_offset + 13, 1) * 512;
    sector_t number_of_reserved_sectors = storage_peek(lba_offset + 14, 2);
    base_fat = lba_offset + number_of_reserved_sectors * 512;
    uint32_t number_of_fats = storage_peek(lba_offset + 16, 1);
    FS_TEST_ASSERT(number_of_fats == 1 || number_of_fats == 2);
    max_root_dir_entries = storage_peek(lba_offset + 17, 2);
    sector_t total_logical_sectors = storage_peek(lba_offset + 19, 2);
    sector_t logical_sectors_per_fat = storage_peek(lba_offset + 22, 2);
    base_dir = base_fat + logical_sectors_per_fat * number_of_fats * 512;
    base_data = base_dir + ((max_root_dir_entries * 32 + 511) / 512) * 512;

    FS_DBG("number_of_reserved_sectors=%d", number_of_reserved_sectors);
    FS_DBG("number_of_fats=%d", number_of_fats);
    FS_DBG("max_root_dir_entries=%d", max_root_dir_entries);
    FS_DBG("total_logical_sectors=%d", total_logical_sectors);
    FS_DBG("logical_sectors_per_fat=%d", logical_sectors_per_fat);

    FS_DBG("cluster_size=%d", cluster_size);
    FS_DBG("base_fat=%d", base_fat);
    FS_DBG("base_dir=%d", base_dir);
    FS_DBG("base_data=%d", base_data);

    mounted = true;
    return 0;
}

int F12RFileSystem::unmount() {
    mounted = false;
    return 0;
}

uint32_t F12RFileSystem::storage_peek(uint32_t offset, int n) {
    uint8_t buf[n];
    storage->Read(offset, buf, sizeof(buf));
    uint32_t val = 0;
    for(int i = 0; i < n; i++) {
        val |= buf[i]<<(i*8);
    }
    return val;
}

cluster_t F12RFileSystem::fat_read(cluster_t index) {
    int i = index / 2 * 3 + (index&1);
    cluster_t next = storage_peek(base_fat + i, 2);
    if (index & 1) {
        next >>= 4;
    } else {
        next &= 0xfff;
    }
    return next;
}

